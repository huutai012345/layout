import React, { Component } from "react";
import "./App.css";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Register from "./components/Register";
import Services from "./components/Services";
import Companies from "./components/Companies";
import Contact from "./components/Contact";
import Banner from "./components/Banner";
import Targets from "./components/Targets";

class App extends Component {
  render() {
    return (
      <div className="container rz">
        <Header />
        <Banner />

        <main>
          <Companies />
          <Services />
          <Targets />
          <Register />
          <Contact />
        </main>

        <Footer />
      </div>
    );
  }
}

export default App;
