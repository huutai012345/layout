import React, { Component } from "react";

class Banner extends Component {
  render() {
    return (
      <div className="row">
        <img
          src="https://i0.wp.com/codegym.vn/wp-content/uploads/2020/05/reactjs-tim-hieu-ve-component-api-8.png?fit=1280%2C720&ssl=1"
          className="img-responsive"
          alt=""
        />
        <hr />
      </div>
    );
  }
}

export default Banner;
