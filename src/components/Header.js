import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <a className="navbar-brand" href="#">
              shortlist
            </a>
          </div>
          <div>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <a href="#">about us</a>
              </li>
              <li>
                <a href="#">services</a>
              </li>
              <li>
                <a href="#">blog</a>
              </li>
              <li>
                <a href="#">contact us</a>
              </li>
              <li>
                <a className="spe" href="#">
                  {" "}
                  SHORTLISt METRICS
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;
