import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer className="row">
        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <h3>shortlist</h3>
          <p>Your growth marketing partner</p>
          <p>
            <strong>About Us | Shortlist | Blog | Contact Us</strong>
          </p>
          <p>
            SERVICES:{" "}
            <strong>
              Link Building | SEO Service | Web Design and Development | Done
              For You Marketing
            </strong>
          </p>
          <p>
            PARTNERS: <strong> LTVPlus |TasksDrive | How We Solve </strong>
          </p>
          <p>
            We help boost revenues for business like through proven strategies
            that will turn your website into a lead-attracting,
            profit-generating machine
          </p>
        </div>
        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <div>
            <span className="fab fa-facebook-square"></span> &nbsp;
            <span className="fab fa-invision"></span>
          </div>
          <p>
            <strong>Disclaimer | Terms of Service | Privacy Policy</strong>
          </p>
          <p>Copyright C 2020 Shortlist</p>
        </div>
      </footer>
    );
  }
}

export default Footer;
