import React, { Component } from "react";

class Contact extends Component {
  render() {
    return (
      <div className="row text-center mt-10">
        <h2>Ready to get shortlisted all the way to the top ?</h2>
        <br />
        <h4>Just tell us what you need.</h4>
        <br />
        <button type="button" className="btn btn-danger mb-10">
          I'M INTERESTED. LET'S SCHEDULE A CALL
        </button>
        <hr />
      </div>
    );
  }
}

export default Contact;
