import React, { Component } from "react";

class Register extends Component {
  render() {
    return (
      <div className="row mt-10">
        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div className="text-center">
            <p>SIGN UP FOR OUR NEWSLETTER</p>
            <br />
            <h2>Join our newsletter waitlist</h2>
            <br />
            <p>
              Add get subscriber-only first access to our case studies before
              everyone else
            </p>
            <br />
            <div className="circle">
              <div className="circle3x"></div>
            </div>
            <div className="circle">
              <div className="circle2x"></div>
            </div>
            <div className="circle">
              <div className="circle1x"></div>
            </div>

            <div className="form-group">
              <input type="text" className="form-control" name="name" />
            </div>
            <br />
            <div className="text-center">
              <button type="submit" className="btn btn-success">
                SUBSCRIBE TO GET EXCLUSIVE ACCESS
              </button>
            </div>
          </div>
        </div>
        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <img
            src="https://cdn.pixabay.com/photo/2019/10/19/17/24/gmail-4561841_960_720.png"
            className="img-responsive"
            alt=""
          />
        </div>
      </div>
    );
  }
}

export default Register;
