import React, { Component } from "react";

class Services extends Component {
  render() {
    return (
      <div className="row text-center">
        <h2>SERVICES</h2>
        <br></br>
        <br></br>
        <h4>
          Why lose your precious time and resources doing everything yourself
        </h4>
        <div className="row mt-10">
          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <span className="fa fa-building fa-7x"></span>
            </div>
            <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <h4>Link Building</h4>
              <br />
              <p>
                We'll earn white hat backlinks and send qualified traffic to
                your business through guest posting
              </p>
              <br />
              <button type="button" className="btn btn-success">
                LEARN MORE
              </button>
            </div>
          </div>
          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <span className="fa fa-pen fa-7x"></span>
            </div>
            <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <h4>SEO Servies</h4>
              <br />
              <p>
                From content creation to consulting and keyword research, we can
                help you outrank your competition
              </p>
              <br />
              <button type="button" className="btn btn-success">
                LEARN MORE
              </button>
            </div>
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <span className="fa fa-pen fa-7x"></span>
            </div>
            <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <h4>Web Design & Development</h4>
              <br />
              <p>
                We make websites that don't just look great, but are also
                optimized for UX, conversions, and search engine visibility.
              </p>
              <br />
              <button type="button" className="btn btn-success">
                LEARN MORE
              </button>
            </div>
          </div>
          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <span className="fa fa-building fa-7x"></span>
            </div>
            <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <h4>Done-For-You Marketing</h4>
              <br />
              <p>
                Outsource all your marketing needs to a dedicated marketing team
                handpicked just for you business
              </p>
              <br />
              <button type="button" className="btn btn-success">
                LEARN MORE
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Services;
