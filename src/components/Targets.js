import React, { Component } from "react";

class Targets extends Component {
  render() {
    return (
      <div className="row text-center mt-10">
        <h2>
          With us as your dedicated marketing team, you can sit back and watch
          as we help you...
        </h2>
        <div className="row mt-10">
          <div className="col-sm-4">
            <span className="fa fa-sitemap fa-5x"></span>
            <h4>GET MORE BUSINESS LEADS</h4>
            <p>
              Our tailored strategies will send better leads to your business
            </p>
          </div>
          <div className="col-sm-4">
            <span className="fa fa-star fa-5x"></span>
            <h4>OUTRANK THE COMPETITION</h4>
            <p>Get on the lst page of Google for the keywords that matter</p>
          </div>
          <div className="col-sm-4">
            <span className="fa fa-map-marker-alt fa-5x"></span>
            <h4>BE EVERYWHERE</h4>
            <p>
              Always stay connected to your audience wherever they may be online
            </p>
          </div>
        </div>
        <br />
        <br />
        <div className="row ">
          <div className="col-sm-4">
            <span className="fa fa-handshake fa-5x"></span>
            <h4>BECOME THE CHOICE</h4>
            <p>
              Build an online presence that will make you the brand of choice
            </p>
          </div>
          <div className="col-sm-4">
            <span className="fa fa-thumbs-up fa-5x"></span>
            <h4>LOOK GREAT, SELL MORE</h4>
            <p>Get digital assets that attract and drive conversions</p>
          </div>
          <div className="col-sm-4">
            <span className="fa fa-dollar-sign fa-5x"></span>
            <h4>TURN VISTORS INTO BUYERS</h4>
            <p>And turn conversions into repeat purchase.</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Targets;
