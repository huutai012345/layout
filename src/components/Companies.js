import React, { Component } from "react";

class Companies extends Component {
  render() {
    return (
      <div className="row text-center">
        <h4>These companies trust us with their marketing</h4>
        <br />
        <br />
        <div className="row">
          <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <p>
              <span className="fab fa-adn fa-3x"></span>Sister
            </p>
          </div>
          <div className="col-xs-3 col-sm-2 col-md-2 col-lg-2">
            <p>
              <span className="fab fa-adn fa-3x"></span>NameSilo
            </p>
          </div>
          <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <p>
              <span className="fab fa-adn fa-3x"></span>Relote
            </p>
          </div>
          <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <p>
              <span className="fab fa-adn fa-3x"></span>NEUROPEDICS
            </p>
          </div>
          <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <p>
              <span className="fab fa-adn fa-3x"></span>ART of CHARM
            </p>
          </div>
        </div>
        <hr />
      </div>
    );
  }
}

export default Companies;
